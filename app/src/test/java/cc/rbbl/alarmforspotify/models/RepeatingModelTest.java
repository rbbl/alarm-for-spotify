package cc.rbbl.alarmforspotify.models;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class RepeatingModelTest {

    private RepeatingModel mondaySaturday;
    private static final int MONDAY_SATURDAY_ACTIVE = 66;
    private static final int FAKE_MONDAY_SATURDAY_ACTIVE = -62;
    private static final int MONDAY_WEDNESDAY_SATURDAY_ACTIVE = 82;

    @Before
    public void setUp() throws Exception {
        mondaySaturday = new RepeatingModel(MONDAY_SATURDAY_ACTIVE);
    }

    @Test
    public void equals() {
        RepeatingModel model = new RepeatingModel(MONDAY_SATURDAY_ACTIVE);
        assertEquals(mondaySaturday, model);
    }

    @Test
    public void equalsNegative() {
        RepeatingModel model = new RepeatingModel(MONDAY_WEDNESDAY_SATURDAY_ACTIVE);
        assertNotEquals(mondaySaturday, model);
    }

    @Test
    public void initialization() {
        RepeatingModel model = new RepeatingModel(FAKE_MONDAY_SATURDAY_ACTIVE);
        assertEquals(mondaySaturday.toByte(), model.toByte());
    }

    @Test
    public void isActive() {
        assertTrue(mondaySaturday.isActive(RepeatingModel.WeekDays.MONDAY));
    }

    @Test
    public void isActive2() {
        assertTrue(mondaySaturday.isActive(RepeatingModel.WeekDays.SATURDAY));
    }

    @Test
    public void isInactive() {
        assertFalse(mondaySaturday.isActive(RepeatingModel.WeekDays.TUESDAY));
    }

    @Test
    public void isInactive2() {
        assertFalse(mondaySaturday.isActive(RepeatingModel.WeekDays.THURSDAY));
    }

    @Test
    public void setActive1() {
        mondaySaturday.setActive(RepeatingModel.WeekDays.TUESDAY);
        assertTrue(mondaySaturday.isActive(RepeatingModel.WeekDays.TUESDAY));
    }

    @Test
    public void setActive2() {
        mondaySaturday.setActive(RepeatingModel.WeekDays.THURSDAY);
        assertTrue(mondaySaturday.isActive(RepeatingModel.WeekDays.THURSDAY));
    }

    @Test
    public void setInactive1() {
        mondaySaturday.setInactive(RepeatingModel.WeekDays.MONDAY);
        assertFalse(mondaySaturday.isActive(RepeatingModel.WeekDays.MONDAY));
    }

    @Test
    public void setInactive2() {
        mondaySaturday.setInactive(RepeatingModel.WeekDays.SATURDAY);
        assertFalse(mondaySaturday.isActive(RepeatingModel.WeekDays.SATURDAY));
    }

    @Test
    public void setActiveBool1() {
        mondaySaturday.setActive(RepeatingModel.WeekDays.SATURDAY, false);
        assertFalse(mondaySaturday.isActive(RepeatingModel.WeekDays.SATURDAY));
    }

    @Test
    public void setActiveBool2() {
        mondaySaturday.setActive(RepeatingModel.WeekDays.SATURDAY, true);
        assertTrue(mondaySaturday.isActive(RepeatingModel.WeekDays.SATURDAY));
    }

    @Test
    public void isRepeating1() {
        assertTrue(mondaySaturday.isRepeating());
    }

    @Test
    public void isRepeating2() {
        assertFalse(new RepeatingModel(0).isRepeating());
    }

    @Test
    public void toByte1() {
        assertEquals(MONDAY_SATURDAY_ACTIVE, mondaySaturday.toByte());
    }

    @Test
    public void toByte2() {
        mondaySaturday.setActive(RepeatingModel.WeekDays.WEDNESDAY);
        assertEquals(MONDAY_WEDNESDAY_SATURDAY_ACTIVE, mondaySaturday.toByte());
    }

    @Test
    public void iterate() {
        boolean[] expected = new boolean[]{true, false, false, false, false, true, false};
        boolean[] actual = new boolean[7];
        int i = 0;
        for(boolean active : mondaySaturday){
            actual[i] = active;
            i++;
        }
        assertArrayEquals(expected, actual);
    }

    @Test
    public void iterate2() {
        RepeatingModel saturdaySunday = new RepeatingModel(3);
        boolean[] expected = new boolean[]{false, false, false, false, false, true, true};
        boolean[] actual = new boolean[7];
        int i = 0;
        for(boolean active : saturdaySunday){
            actual[i] = active;
            i++;
        }
        assertArrayEquals(expected, actual);
    }

    @Test
    public void iterateTwice() {
        boolean[] expected = new boolean[]{true, false, false, false, false, true, false};
        boolean[] actualOne = new boolean[7];
        boolean[] actualTwo = new boolean[7];
        int i = 0;
        for(boolean active : mondaySaturday){
            actualOne[i] = active;
            i++;
        }
        i = 0;
        for(boolean active : mondaySaturday){
            actualTwo[i] = active;
            i++;
        }
        assertArrayEquals(expected, actualOne);
        assertArrayEquals(expected, actualTwo);
    }
}