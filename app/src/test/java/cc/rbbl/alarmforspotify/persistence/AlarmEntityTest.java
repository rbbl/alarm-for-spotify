package cc.rbbl.alarmforspotify.persistence;

import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.*;

public class AlarmEntityTest {

    @Test
    public void testEquals() {
        LocalTime timeOne =  LocalTime.of(20, 15);
        AlarmEntity entityOne = new AlarmEntity(timeOne, true);
        AlarmEntity entityTwo = new AlarmEntity(timeOne, true);
        assertEquals(entityOne, entityTwo);
    }
}