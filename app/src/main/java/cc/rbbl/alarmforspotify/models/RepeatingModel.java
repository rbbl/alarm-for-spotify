package cc.rbbl.alarmforspotify.models;

import androidx.annotation.NonNull;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class RepeatingModel implements Iterable<Boolean>, Iterator<Boolean> {
    private byte activeWeekDays;
    private int currentDay = 0;

    private static final byte FAKE_INACTIVE_WEEK = -128;
    private static final byte INACTIVE_WEEK = 0;

    private static final byte MONDAY_ACTIVE = 64;

    public enum WeekDays {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
    }

    public RepeatingModel() {
    }

    public RepeatingModel(byte input) {
        //unset the first bit if needed
        if (INACTIVE_WEEK == (input & FAKE_INACTIVE_WEEK)) {
            activeWeekDays = input;
        } else {
            activeWeekDays = (byte) (input ^ FAKE_INACTIVE_WEEK);
        }
    }

    public RepeatingModel(int input) {
        this((byte) input);
    }

    public boolean isActive(WeekDays weekDay) {
        byte dayActive = (byte) (MONDAY_ACTIVE >> weekDay.ordinal());
        return dayActive == (dayActive & activeWeekDays);
    }

    public void setActive(WeekDays weekDay) {
        if (!isActive(weekDay)) {
            activeWeekDays = (byte) ((MONDAY_ACTIVE >> weekDay.ordinal()) | activeWeekDays);
        }
    }

    public void setInactive(WeekDays weekDay) {
        if (isActive(weekDay)) {
            activeWeekDays = (byte) ((MONDAY_ACTIVE >> weekDay.ordinal()) ^ activeWeekDays);
        }
    }

    public void setActive(WeekDays weekDay, boolean value) {
        if (value) {
            setActive(weekDay);
        } else {
            setInactive(weekDay);
        }
    }

    public boolean isRepeating() {
        return activeWeekDays != 0;
    }

    public byte toByte() {
        return activeWeekDays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RepeatingModel repeatingModel = (RepeatingModel) o;
        return activeWeekDays == repeatingModel.activeWeekDays;
    }

    @Override
    public int hashCode() {
        return Objects.hash(activeWeekDays);
    }

    @NonNull
    @Override
    public Iterator<Boolean> iterator() {
        currentDay = 0;
        return this;
    }

    @Override
    public boolean hasNext() {
        return this.currentDay < WeekDays.values().length;
    }

    @Override
    public Boolean next() {
        if (currentDay >= WeekDays.values().length) {
            throw new NoSuchElementException();
        }
        boolean result = isActive(WeekDays.values()[currentDay]);
        currentDay++;
        return result;
    }
}
