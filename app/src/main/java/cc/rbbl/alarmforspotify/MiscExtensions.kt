package cc.rbbl.alarmforspotify

import com.adamratzman.spotify.models.*

fun AlbumUri.Companion.typeAsString() = "album"

fun ArtistUri.Companion.typeAsString() = "artist"

fun EpisodeUri.Companion.typeAsString() = "episode"

fun PlaylistUri.Companion.typeAsString() = "playlist"

fun ShowUri.Companion.typeAsString() = "show"

fun SpotifyTrackUri.Companion.typeAsString() = "track"
