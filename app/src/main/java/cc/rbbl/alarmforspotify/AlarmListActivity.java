package cc.rbbl.alarmforspotify;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import cc.rbbl.alarmforspotify.databinding.ActivityAlarmListBinding;
import cc.rbbl.alarmforspotify.databinding.SelectlistListItemBinding;
import cc.rbbl.alarmforspotify.spotify.SpotifyConsentActivity;
import cc.rbbl.alarmforspotify.spotify.SpotifyCredentialStoreHolder;
import cc.rbbl.alarmforspotify.viewmodels.AlarmListViewModel;

public class AlarmListActivity extends AppCompatActivity {

    AlarmListViewModel alarmListViewModel;

    private ActivityAlarmListBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAlarmListBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        alarmListViewModel = new ViewModelProvider(this).get(AlarmListViewModel.class);
        RecyclerView recyclerView = binding.alarmsList;
        final AlarmListAdapter adapter = new AlarmListAdapter(alarmListViewModel);
        recyclerView.setAdapter(adapter);
        alarmListViewModel.getAlarms().observe(this, adapter::updateAlarmListItems);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new AlarmListAdapter.SwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);
        if (!isSpotifyEnabled()) {
            startActivity(new Intent(this, SpotifyConsentActivity.class));
        }
        alarmListViewModel.reFetchData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isSpotifyEnabled()) {
            startActivity(new Intent(this, SpotifyConsentActivity.class));
        }
        alarmListViewModel.reFetchData();
    }

    public void newAlarm(View view) {
        Intent intent = new Intent(AlarmListActivity.this, AlarmEditActivity.class);
        startActivity(intent);
    }

    private boolean isSpotifyEnabled() {
        return SpotifyCredentialStoreHolder.getCredentialStoreInstance(getApplication().getApplicationContext()).canApiBeRefreshed();
    }
}