package cc.rbbl.alarmforspotify.spotify.browser

import cc.rbbl.alarmforspotify.typeAsString
import com.adamratzman.spotify.models.EpisodeUri

class LibraryEpisodesFragment : AbstractSpotifySelectionListFragment() {
    override fun setAdapterUpdate(adapter: SpotifySelectionListAdapter) {
        spotifyBrowserViewModel.getSavedEpisodes().observe(
            viewLifecycleOwner,
            { newItemList: List<SimpleSpotifyListItem?>? -> adapter.updateList(newItemList) })
    }

    override fun fetchListItems(limit: Int, offset: Int) {
        spotifyBrowserViewModel.fetchOffsetList(limit, offset, EpisodeUri.typeAsString())
    }

    override fun getItemListSize(): Int {
        return spotifyBrowserViewModel.getSavedEpisodes().value!!.size
    }
}