package cc.rbbl.alarmforspotify.spotify.browser;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import org.jetbrains.annotations.NotNull;

import cc.rbbl.alarmforspotify.R;
import cc.rbbl.alarmforspotify.databinding.FragmentLibraryBinding;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class LibraryFragment extends Fragment {

    private static final String TAB_PREFERENCE = "cc.rbbl.alarmforspotify.spotify.browser.LibraryFragment.tabReference";

    private static final int NUM_PAGES = 6;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentLibraryBinding binding = FragmentLibraryBinding.inflate(inflater, container, false);
        binding.libraryViewPager.setSaveEnabled(false);
        ScreenSlidePagerAdapter pagerAdapter = new ScreenSlidePagerAdapter(this);
        binding.libraryViewPager.setAdapter(pagerAdapter);
        new TabLayoutMediator(binding.libraryTabLayout, binding.libraryViewPager, (tab, position) -> {
            switch (position) {
                case 0:
                    tab.setText(R.string.playlists);
                    break;
                case 1:
                    tab.setText(R.string.artists);
                    break;
                case 2:
                    tab.setText(R.string.albums);
                    break;
                case 3:
                    tab.setText(R.string.tracks);
                    break;
                case 4:
                    tab.setText(R.string.episodes);
                    break;
                case 5:
                    tab.setText(R.string.shows);
                    break;
            }
        }).attach();
        binding.libraryTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                storeTabPreference(tab.getPosition());
                Log.i(this.getClass().getName(), "Tab Position: " + tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        binding.libraryTabLayout.selectTab(binding.libraryTabLayout.getTabAt(getTabPreference()));
        return binding.getRoot();
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStateAdapter {
        public ScreenSlidePagerAdapter(Fragment fa) {
            super(fa);
        }

        @NotNull
        @Override
        public Fragment createFragment(int position) {
            Log.d(this.getClass().getName(), "ScreenPage Position: " + position);
            switch (position) {
                case 0:
                    return new LibraryPlaylistsFragment();
                case 1:
                    return new LibraryFollowedArtistsFragment();
                case 2:
                    return new LibrarySavedAlbumsFragment();
                case 3:
                    return new LibrarySavedTracksFragment();
                case 4:
                    return new LibraryEpisodesFragment();
                case 5:
                    return new LibraryShowsFragment();
                default:
                    throw new RuntimeException("Unexpected Page Number");
            }
        }

        @Override
        public int getItemCount() {
            return NUM_PAGES;
        }
    }

    private void storeTabPreference(int id) {
        SharedPreferences preferences = requireActivity().getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(TAB_PREFERENCE, id);
        editor.apply();
    }

    private int getTabPreference() {
        SharedPreferences preferences = requireActivity().getPreferences(MODE_PRIVATE);
        return preferences.getInt(TAB_PREFERENCE, 0);
    }

}