package cc.rbbl.alarmforspotify.spotify.browser

import cc.rbbl.alarmforspotify.typeAsString
import com.adamratzman.spotify.models.SpotifyTrackUri

class LibrarySavedTracksFragment : AbstractSpotifySelectionListFragment() {
    override fun setAdapterUpdate(adapter: SpotifySelectionListAdapter) {
        spotifyBrowserViewModel.getSavedTracks().observe(
            this,
            { newItemList: List<SimpleSpotifyListItem?>? -> adapter.updateList(newItemList) })
    }

    override fun fetchListItems(limit: Int, offset: Int) {
        spotifyBrowserViewModel.fetchOffsetList(limit, offset, SpotifyTrackUri.typeAsString())
    }

    override fun getItemListSize(): Int {
        return spotifyBrowserViewModel.getSavedTracks().value!!.size
    }
}