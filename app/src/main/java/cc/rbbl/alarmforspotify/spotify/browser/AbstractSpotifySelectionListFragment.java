package cc.rbbl.alarmforspotify.spotify.browser;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import cc.rbbl.alarmforspotify.databinding.FragmentSelectItemListBinding;
import cc.rbbl.alarmforspotify.spotify.browser.viewmodels.SpotifyBrowserViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class AbstractSpotifySelectionListFragment extends Fragment {

    private static final int PAGE_SIZE = 50;

    protected SpotifyBrowserViewModel spotifyBrowserViewModel;

    public AbstractSpotifySelectionListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentSelectItemListBinding binding = FragmentSelectItemListBinding.inflate(inflater, container, false);
        spotifyBrowserViewModel = new ViewModelProvider(requireActivity()).get(SpotifyBrowserViewModel.class);
        SpotifySelectionListAdapter adapter = new SpotifySelectionListAdapter(spotifyBrowserViewModel, getParentFragmentManager());
        binding.playlistRecyclerView.setAdapter(adapter);
        binding.playlistRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        setAdapterUpdate(adapter);
        fetchListItems(PAGE_SIZE, 0);
        binding.playlistRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_SETTLING || newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    if (getItemListSize() - linearLayoutManager.findLastVisibleItemPosition() < PAGE_SIZE) {
                        fetchListItems(PAGE_SIZE, getItemListSize());
                    }
                }
            }
        });
        return binding.getRoot();
    }

    protected abstract void setAdapterUpdate(SpotifySelectionListAdapter adapter);

    protected abstract void fetchListItems(int limit, int offset);

    protected abstract int getItemListSize();
}