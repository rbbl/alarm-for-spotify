package cc.rbbl.alarmforspotify.spotify;

import android.content.Context;

import com.adamratzman.spotify.auth.SpotifyDefaultCredentialStore;

public class SpotifyCredentialStoreHolder {
    private static SpotifyDefaultCredentialStore instance;

    public static SpotifyDefaultCredentialStore getCredentialStoreInstance(Context applicationContext) {
        if (SpotifyCredentialStoreHolder.instance == null) {
            SpotifyCredentialStoreHolder.instance = new SpotifyDefaultCredentialStore(
                    "059bcb4adc0746929455fb3d1154bb56",
                    //"http://rbbl.cc/test",
                    "alarmforspotify://spotify-pkce",
                    applicationContext);
        }
        return SpotifyCredentialStoreHolder.instance;
    }
}
