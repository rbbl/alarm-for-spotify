package cc.rbbl.alarmforspotify.spotify.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import cc.rbbl.alarmforspotify.spotify.SpotifyCredentialStoreHolder
import com.adamratzman.spotify.auth.SpotifyDefaultCredentialStore
import java.util.*

class SpotifyConsentViewModel(application: Application) : AndroidViewModel(application) {
    val clientId = "059bcb4adc0746929455fb3d1154bb56" //todo replace with build parameter
    val redirectUri = "alarmforspotify://spotify-pkce" //todo replace with build parameter
    val pkceCodeVerifier = (0..96).joinToString("") {
        (('a'..'z') + ('A'..'Z') + ('0'..'9')).random().toString();
    }
    val state = Random().nextLong().toString()

    val credentialStore: SpotifyDefaultCredentialStore = SpotifyCredentialStoreHolder.getCredentialStoreInstance(application.applicationContext)

}