package cc.rbbl.alarmforspotify.spotify.browser

import android.graphics.Bitmap
import com.adamratzman.spotify.models.*

data class SimpleSpotifyListItem(
    val uri: SpotifyUri,
    val title: String,
    val creator: String,
    val imagesData: List<SpotifyImage>,
    var image: Bitmap? = null
) {
    constructor(simplePlaylist: SimplePlaylist, image: Bitmap? = null) : this(
        simplePlaylist.uri,
        simplePlaylist.name,
        simplePlaylist.owner.displayName ?: "",
        simplePlaylist.images,
        image
    )

    constructor(savedAlbum: SavedAlbum, image: Bitmap? = null) : this(
        savedAlbum.album.uri,
        savedAlbum.album.name,
        savedAlbum.album.artists.toArtistString(),
        savedAlbum.album.images,
        image
    )

    constructor(artist: Artist, image: Bitmap? = null) : this(
        artist.uri,
        artist.name,
        "Followers: ${artist.followers.total.toString()}",
        artist.images,
        image
    )

    constructor(track: SavedTrack, image: Bitmap? = null) : this(
        track.track.uri,
        track.track.name,
        track.track.artists.toArtistString(),
        track.track.album.images,
        image
    )

    constructor(episode: SavedEpisode, image: Bitmap? = null) : this(
        episode.episode.uri,
        episode.episode.name,
        episode.episode.show.name,
        episode.episode.images,
        image
    )

    constructor(show: SavedShow, image: Bitmap? = null) : this(
        show.show.uri,
        show.show.name,
        show.show.publisher,
        show.show.images,
        image
    )

    constructor(source: SimpleSpotifyListItem, image: Bitmap? = null) : this(
        source.uri,
        source.title,
        source.creator,
        source.imagesData,
        image ?: source.image
    )
}

fun List<SimpleArtist>.toArtistString(): String {
    var artistString = ""
    for (artist in this) {
        if (artistString == "") {
            artistString = artist.name
        } else {
            artistString += ", " + artist.name
        }
    }
    return artistString
}
