package cc.rbbl.alarmforspotify.spotify.browser.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import cc.rbbl.alarmforspotify.spotify.SpotifyCredentialStoreHolder
import cc.rbbl.alarmforspotify.spotify.browser.ImageRepository
import cc.rbbl.alarmforspotify.spotify.browser.ListFetchJob
import cc.rbbl.alarmforspotify.spotify.browser.SimpleSpotifyListItem
import cc.rbbl.alarmforspotify.typeAsString
import com.adamratzman.spotify.SpotifyClientApi
import com.adamratzman.spotify.SpotifyException
import com.adamratzman.spotify.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SpotifyBrowserViewModel(application: Application) : AndroidViewModel(application) {
    private var mSpotifyApi: SpotifyClientApi? =
        SpotifyCredentialStoreHolder.getCredentialStoreInstance(getApplication<Application>().applicationContext)
            .getSpotifyClientPkceApi(null)

    private val personalPlaylists =
        MutableLiveData<ArrayList<SimpleSpotifyListItem>>().apply { value = ArrayList() }
    private var lastPersonalPlaylistsJob: ListFetchJob? = null

    private val followedArtists =
        MutableLiveData<ArrayList<SimpleSpotifyListItem>>().apply { value = ArrayList() }
    private var lastFollowedArtistsJob: Job? = null
    private var followedArtistsTotalLeft: Int? = null

    private val savedAlbums =
        MutableLiveData<ArrayList<SimpleSpotifyListItem>>().apply { value = ArrayList() }
    private var lastSavedAlbumsJob: ListFetchJob? = null

    private val savedTracks =
        MutableLiveData<ArrayList<SimpleSpotifyListItem>>().apply { value = ArrayList() }
    private var lastSavedTracksJob: ListFetchJob? = null

    private val savedEpisodes =
        MutableLiveData<ArrayList<SimpleSpotifyListItem>>().apply { value = ArrayList() }
    private var lastSavedEpisodesJob: ListFetchJob? = null

    private val savedShows =
        MutableLiveData<ArrayList<SimpleSpotifyListItem>>().apply { value = ArrayList() }
    private var lastSavedShowsJob: ListFetchJob? = null

    val imageRepository = ImageRepository(getApplication<Application>().applicationContext)

    fun fetchOffsetList(limit: Int, offset: Int, uriType: String) {
        val lastJob: ListFetchJob? = when (uriType) {
            AlbumUri.typeAsString() -> lastSavedAlbumsJob
            EpisodeUri.typeAsString() -> lastSavedEpisodesJob
            PlaylistUri.typeAsString() -> lastPersonalPlaylistsJob
            SpotifyTrackUri.typeAsString() -> lastSavedTracksJob
            ShowUri.typeAsString() -> lastSavedShowsJob
            else -> throw IllegalArgumentException("Unsupported Type: $uriType")
        }
        if (lastJob != null) {
            if (offset <= lastJob.offSet) {
                return
            }
            if (lastJob.job.isActive) {
                return
            }
        }
        Log.d(this.javaClass.name, "Fetching personal Playlists: Limit: $limit Offset: $offset")
        val job = viewModelScope.launch {
            try {
                val liveData: MutableLiveData<ArrayList<SimpleSpotifyListItem>>
                val newItems: List<SimpleSpotifyListItem>
                val total: Int
                when (uriType) {
                    AlbumUri.typeAsString() -> {
                        val newAlbums = mSpotifyApi?.library?.getSavedAlbums(limit, offset)!!
                        newItems = newAlbums.items.map {
                            SimpleSpotifyListItem(
                                it,
                                imageRepository.imageCache[it.album.uri]
                            )
                        }
                        liveData = savedAlbums
                        total = newAlbums.total
                    }
                    EpisodeUri.typeAsString() -> {
                        val newEpisodes = mSpotifyApi?.library?.getSavedEpisodes(limit, offset)!!
                        newItems = newEpisodes.items.map {
                            SimpleSpotifyListItem(
                                it,
                                imageRepository.imageCache[it.episode.uri]
                            )
                        }
                        liveData = savedEpisodes
                        total = newEpisodes.total
                    }
                    PlaylistUri.typeAsString() -> {
                        val newPlaylists =
                            mSpotifyApi?.playlists?.getClientPlaylists(limit, offset)!!
                        newItems = newPlaylists.items.map {
                            SimpleSpotifyListItem(
                                it,
                                imageRepository.imageCache[it.uri]
                            )
                        }
                        liveData = personalPlaylists
                        total = newPlaylists.total
                    }
                    SpotifyTrackUri.typeAsString() -> {
                        val newTracks = mSpotifyApi?.library?.getSavedTracks(limit, offset)!!
                        newItems = newTracks.items.map {
                            SimpleSpotifyListItem(
                                it,
                                imageRepository.imageCache[it.track.uri]
                            )
                        }
                        liveData = savedTracks
                        total = newTracks.total
                    }
                    ShowUri.typeAsString() -> {
                        val newShows = mSpotifyApi?.library?.getSavedShows(limit, offset)!!
                        newItems = newShows.items.map {
                            SimpleSpotifyListItem(
                                it,
                                imageRepository.imageCache[it.show.uri]
                            )
                        }
                        liveData = savedShows
                        total = newShows.total
                    }
                    else -> throw IllegalArgumentException("Unsupported Type: $uriType")
                }
                liveData.postValue(liveData.value!!.apply { addAll(newItems) })
                Log.d(
                    this.javaClass.name,
                    "Fetching Items Total: $total \t current: ${liveData.value!!.size}"
                )
            } catch (e: SpotifyException.BadRequestException) {
                Log.e(this.javaClass.name, "Error during fetching items", e)
            }
        }
        when (uriType) {
            AlbumUri.typeAsString() -> lastSavedAlbumsJob = ListFetchJob(offset, job)
            EpisodeUri.typeAsString() -> lastSavedEpisodesJob = ListFetchJob(offset, job)
            PlaylistUri.typeAsString() -> lastPersonalPlaylistsJob = ListFetchJob(offset, job)
            SpotifyTrackUri.typeAsString() -> lastSavedTracksJob = ListFetchJob(offset, job)
            ShowUri.typeAsString() -> lastSavedShowsJob = ListFetchJob(offset, job)
            else -> throw IllegalArgumentException("Unsupported Type: $uriType")
        }
    }

    fun getPersonalPlaylists(): LiveData<List<SimpleSpotifyListItem>> {
        return personalPlaylists as LiveData<List<SimpleSpotifyListItem>>
    }

    fun fetchFollowedArtists(limit: Int) {
        if (lastFollowedArtistsJob != null) {
            if (lastFollowedArtistsJob!!.isActive) {
                return
            }
        }
        if (followedArtistsTotalLeft == 0) {
            return
        }
        lastFollowedArtistsJob = viewModelScope.launch {
            try {
                val allArtists = followedArtists.value!!
                Log.d(
                    this.javaClass.name,
                    "Fetching followed Artists: Limit: $limit last_id: ${allArtists.lastOrNull()?.uri?.id}"
                )
                val newArtists =
                    mSpotifyApi?.following?.getFollowedArtists(
                        limit,
                        allArtists.lastOrNull()?.uri?.id
                    )!!
                followedArtistsTotalLeft = newArtists.total
                followedArtists.value = followedArtists.value!!.apply {
                    addAll(newArtists.items.map {
                        SimpleSpotifyListItem(
                            it
                        )
                    })
                }
                Log.d(
                    this.javaClass.name,
                    "Followed Artists Total remaining: ${newArtists.total} \t Followed Artists current: ${allArtists.size}"
                )
            } catch (e: SpotifyException.BadRequestException) {
                Log.e(this.javaClass.name, "Error during artists fetch", e)
            }
        }
    }

    fun getFollowedArtists(): LiveData<List<SimpleSpotifyListItem>> {
        return followedArtists as LiveData<List<SimpleSpotifyListItem>>
    }

    fun getSavedAlbums(): LiveData<List<SimpleSpotifyListItem>> {
        return savedAlbums as LiveData<List<SimpleSpotifyListItem>>
    }

    fun getSavedTracks(): LiveData<List<SimpleSpotifyListItem>> {
        return savedTracks as LiveData<List<SimpleSpotifyListItem>>
    }

    fun getSavedEpisodes(): LiveData<List<SimpleSpotifyListItem>> {
        return savedEpisodes as LiveData<List<SimpleSpotifyListItem>>
    }

    fun getSavedShows(): LiveData<List<SimpleSpotifyListItem>> {
        return savedShows as LiveData<List<SimpleSpotifyListItem>>
    }

    fun fetchImage(uri: SpotifyUri) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val liveData = when (uri.type) {
                    AlbumUri.typeAsString() -> savedAlbums
                    ArtistUri.typeAsString() -> followedArtists
                    EpisodeUri.typeAsString() -> savedEpisodes
                    PlaylistUri.typeAsString() -> personalPlaylists
                    SpotifyTrackUri.typeAsString() -> savedTracks
                    ShowUri.typeAsString() -> savedShows
                    else -> throw IllegalArgumentException("Unsupported Type: ${uri.type}")
                }
                val targetIndex = liveData.value!!.indexOfFirst { it.uri == uri }
                liveData.value!![targetIndex] = SimpleSpotifyListItem(
                    liveData.value!![targetIndex],
                    imageRepository.getImage(liveData.value!![targetIndex].uri, liveData.value!![targetIndex].imagesData)
                )
                liveData.postValue(liveData.value!!)
            }
        }
    }
}