package cc.rbbl.alarmforspotify.spotify.browser;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cc.rbbl.alarmforspotify.spotify.browser.viewmodels.SpotifyBrowserViewModel;

public class SpotifySelectionListAdapter extends RecyclerView.Adapter<SpotifySelectionListViewHolder> {
    private List<SimpleSpotifyListItem> itemList = new ArrayList<>();
    private final SpotifyBrowserViewModel browserViewModel;
    private final FragmentManager parentFragmentManager;

    public SpotifySelectionListAdapter(SpotifyBrowserViewModel browserViewModel, FragmentManager parentFragmentManager) {
        this.browserViewModel = browserViewModel;
        this.parentFragmentManager = parentFragmentManager;
    }

    @NonNull
    @Override
    public SpotifySelectionListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return SpotifySelectionListViewHolder.create(parent, browserViewModel, parentFragmentManager);
    }

    @Override
    public void onBindViewHolder(@NonNull SpotifySelectionListViewHolder holder, int position) {
        holder.bind(itemList.get(position));
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void updateList(List<SimpleSpotifyListItem> newItemList) {
        if (newItemList == null) {
            return;
        }
        final PlaylistDiff diffCallback = new PlaylistDiff(this.itemList, newItemList);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.itemList = Arrays.asList(new SimpleSpotifyListItem[newItemList.size()].clone());
        Collections.copy(this.itemList, newItemList);
        diffResult.dispatchUpdatesTo(this);
    }

    public static class PlaylistDiff extends DiffUtil.Callback {

        private final List<SimpleSpotifyListItem> oldItemList;
        private final List<SimpleSpotifyListItem> newItemList;

        public PlaylistDiff(List<SimpleSpotifyListItem> oldItemList, List<SimpleSpotifyListItem> newItemList) {
            this.oldItemList = oldItemList;
            this.newItemList = newItemList;
        }

        @Override
        public int getOldListSize() {
            return oldItemList.size();
        }

        @Override
        public int getNewListSize() {
            return newItemList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            if (oldItemList.get(oldItemPosition) == null) {
                return false;
            }
            return oldItemList.get(oldItemPosition).getUri().equals(newItemList.get(
                    newItemPosition).getUri());
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            final SimpleSpotifyListItem oldItem = oldItemList.get(oldItemPosition);
            final SimpleSpotifyListItem newItem = newItemList.get(newItemPosition);

            return oldItem.equals(newItem);
        }

        @Nullable
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            // Implement method if you're going to use ItemAnimator
            return super.getChangePayload(oldItemPosition, newItemPosition);
        }
    }
}
