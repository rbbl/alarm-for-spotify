package cc.rbbl.alarmforspotify.spotify.browser

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import cc.rbbl.alarmforspotify.R
import cc.rbbl.alarmforspotify.databinding.SelectlistListItemBinding
import cc.rbbl.alarmforspotify.spotify.browser.viewmodels.SpotifyBrowserViewModel
import com.adamratzman.spotify.models.SpotifyUri

class SpotifySelectionListViewHolder(
    itemView: View,
    private val browserViewModel: SpotifyBrowserViewModel,
    private val fragmentManager: FragmentManager
) : RecyclerView.ViewHolder(itemView) {
    val binding = SelectlistListItemBinding.bind(itemView)

    private var spotifyUri: SpotifyUri? = null
    private var title: String = ""
    private var creator: String = ""

    init {
        itemView.setOnClickListener {
            SelectItemFragment.newInstance(spotifyUri, title, creator)
                .show(fragmentManager, null);
        }
    }

    fun bind(data: SimpleSpotifyListItem) {
        spotifyUri = data.uri
        title = data.title
        binding.listItemTitle.text = title
        creator = data.creator
        binding.listItemCreator.text = creator
        if (data.image != null) {
            binding.listItemImage.setImageBitmap(data.image)
        } else {
            browserViewModel.fetchImage(spotifyUri!!)
        }
    }


    companion object {
        @JvmStatic
        fun create(
            parent: ViewGroup,
            imageRepository: SpotifyBrowserViewModel,
            parentFragmentManager: FragmentManager
        ): SpotifySelectionListViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.selectlist_list_item, parent, false)
            return SpotifySelectionListViewHolder(view, imageRepository, parentFragmentManager)
        }
    }

}