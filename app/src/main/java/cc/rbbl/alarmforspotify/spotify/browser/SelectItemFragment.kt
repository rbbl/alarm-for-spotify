package cc.rbbl.alarmforspotify.spotify.browser

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import cc.rbbl.alarmforspotify.databinding.FragmentSelectItemBinding
import cc.rbbl.alarmforspotify.spotify.browser.viewmodels.SpotifyBrowserViewModel
import com.adamratzman.spotify.models.SpotifyUri
import com.adamratzman.spotify.models.toSpotifyUri
import kotlinx.coroutines.launch

class SelectItemFragment : DialogFragment() {
    private var _binding: FragmentSelectItemBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    private var mSpotifyUri: String? = null
    private var mTitle: String? = null
    private var mAuthor: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = FragmentSelectItemBinding.inflate(layoutInflater)
        mSpotifyUri = requireArguments().getString(ARG_SPOTIFY_URI)
        mTitle = requireArguments().getString(ARG_TITLE)
        mAuthor = requireArguments().getString(ARG_AUTHOR)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSelectItemBinding.inflate(inflater, container, false)
        val view = binding.root
        val viewModel = ViewModelProvider(requireActivity())[SpotifyBrowserViewModel::class.java]
        lifecycleScope.launch {
            binding.pickItemDialogImage.setImageBitmap(
                viewModel.imageRepository.getImage(
                    mSpotifyUri!!.toSpotifyUri(),
                    null
                )
            )
        }
        binding.pickItemDialogTitle.text = mTitle
        binding.pickItemDialogAuthor.text = mAuthor
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentSelectItemBinding.bind(view)
        binding.cancelButton.setOnClickListener { onClickCancel() }
        binding.selectButton.setOnClickListener { onClickSelect() }
    }

    private fun onClickCancel() {
        dismiss()
    }

    private fun onClickSelect() {
        val resultIntent = Intent()
        resultIntent.putExtra(SpotifyBrowserActivity.ARG_SPOTIFY_URI, mSpotifyUri)
        requireActivity().setResult(Activity.RESULT_OK, resultIntent)
        requireActivity().finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        private const val ARG_SPOTIFY_URI =
            "cc.rbbl.alarmforspotify.spotify.SelectItemFragment.SpotifyURI"
        private const val ARG_TITLE = "cc.rbbl.alarmforspotify.spotify.SelectItemFragment.Title"
        private const val ARG_AUTHOR = "cc.rbbl.alarmforspotify.spotify.SelectItemFragment.Author"
        fun newInstance(uri: SpotifyUri?, title: String?, author: String?): SelectItemFragment {
            val fragment = SelectItemFragment()
            val args = Bundle()
            if (uri != null) {
                args.putString(ARG_SPOTIFY_URI, uri.uri)
            }
            args.putString(ARG_TITLE, title)
            args.putString(ARG_AUTHOR, author)
            fragment.arguments = args
            return fragment
        }
    }
}