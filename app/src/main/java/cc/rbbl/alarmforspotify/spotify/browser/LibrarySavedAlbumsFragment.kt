package cc.rbbl.alarmforspotify.spotify.browser

import cc.rbbl.alarmforspotify.typeAsString
import com.adamratzman.spotify.models.AlbumUri

class LibrarySavedAlbumsFragment : AbstractSpotifySelectionListFragment() {
    override fun setAdapterUpdate(adapter: SpotifySelectionListAdapter) {
        spotifyBrowserViewModel.getSavedAlbums().observe(
            viewLifecycleOwner,
            { newItemList: List<SimpleSpotifyListItem?>? -> adapter.updateList(newItemList) })
    }

    override fun fetchListItems(limit: Int, offset: Int) {
        spotifyBrowserViewModel.fetchOffsetList(limit, offset, AlbumUri.typeAsString())
    }

    override fun getItemListSize(): Int {
        return spotifyBrowserViewModel.getSavedAlbums().value!!.size
    }
}