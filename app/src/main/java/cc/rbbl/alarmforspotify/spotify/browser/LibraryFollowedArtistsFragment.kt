package cc.rbbl.alarmforspotify.spotify.browser

class LibraryFollowedArtistsFragment : AbstractSpotifySelectionListFragment() {
    override fun setAdapterUpdate(adapter: SpotifySelectionListAdapter) {
        spotifyBrowserViewModel.getFollowedArtists().observe(
            this,
            { newItemList: List<SimpleSpotifyListItem?>? -> adapter.updateList(newItemList) })
    }

    override fun fetchListItems(limit: Int, offset: Int) {
        spotifyBrowserViewModel.fetchFollowedArtists(limit)
    }

    override fun getItemListSize(): Int {
        return spotifyBrowserViewModel.getFollowedArtists().value!!.size
    }
}