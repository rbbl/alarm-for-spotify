package cc.rbbl.alarmforspotify.spotify.browser

import kotlinx.coroutines.Job

data class ListFetchJob(val offSet: Int = 0, val job: Job)
