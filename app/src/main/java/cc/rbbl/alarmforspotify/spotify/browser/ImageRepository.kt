package cc.rbbl.alarmforspotify.spotify.browser

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import cc.rbbl.alarmforspotify.spotify.SpotifyCredentialStoreHolder
import cc.rbbl.alarmforspotify.typeAsString
import com.adamratzman.spotify.SpotifyClientApi
import com.adamratzman.spotify.models.*
import kotlinx.coroutines.Job
import java.net.URL

class ImageRepository(context: Context) {
    private val spotifyApi: SpotifyClientApi =
        SpotifyCredentialStoreHolder.getCredentialStoreInstance(context)
            .getSpotifyClientPkceApi(null)!!;
    val imageCache = HashMap<SpotifyUri, Bitmap>()
    private val downloadJobs = HashMap<SpotifyUri, Job>()


    suspend fun getImage(uri: SpotifyUri, images: List<SpotifyImage>?): Bitmap {
        return if (imageCache[uri] != null) {
            imageCache[uri]!!
        } else {
            if (downloadJobs[uri]?.isActive == true) {
                downloadJobs[uri]?.join()
                downloadJobs.remove(uri)
                imageCache[uri]!!
            } else {
                downloadImage(uri, images)
            }
        }
    }

    private suspend fun downloadImage(uri: SpotifyUri, images: List<SpotifyImage>?): Bitmap {
        when (uri.type) {
            AlbumUri.typeAsString() -> {
                return if (images == null) {
                    downloadImage(
                        uri,
                        getOptimalImageUrl(spotifyApi.albums.getAlbum(uri.id)!!.images)
                    )
                } else {
                    downloadImage(uri, getOptimalImageUrl(images))
                }
            }
            ArtistUri.typeAsString() -> {
                return if (images == null) {
                    downloadImage(
                        uri,
                        getOptimalImageUrl(spotifyApi.artists.getArtist(uri.id)!!.images)
                    )
                } else {
                    downloadImage(uri, getOptimalImageUrl(images))
                }
            }
            EpisodeUri.typeAsString() -> {
                return if (images == null) {
                    downloadImage(
                        uri,
                        getOptimalImageUrl(spotifyApi.episodes.getEpisode(uri.id)!!.images)
                    )
                } else {
                    downloadImage(uri, getOptimalImageUrl(images))
                }
            }
            PlaylistUri.typeAsString() -> {
                return if (images == null) {
                    downloadImage(
                        uri,
                        getOptimalImageUrl(spotifyApi.playlists.getClientPlaylist(uri.id)!!.images)
                    )
                } else {
                    downloadImage(uri, getOptimalImageUrl(images))
                }
            }
            ShowUri.typeAsString() -> {
                return if (images == null) {
                    downloadImage(
                        uri,
                        getOptimalImageUrl(spotifyApi.shows.getShow(uri.id)!!.images)
                    )
                } else {
                    downloadImage(uri, getOptimalImageUrl(images))
                }
            }
            SpotifyTrackUri.typeAsString() -> {
                return if (images == null) {
                    val track = spotifyApi.tracks.getTrack(uri.id);
                    val album = spotifyApi.albums.getAlbum(track!!.album.id)
                    val bitmap = downloadImage(uri, getOptimalImageUrl(album!!.images))
                    for (simpleTrack in album.tracks) {
                        imageCache[simpleTrack.uri] = bitmap
                    }
                    bitmap
                } else {
                    downloadImage(uri, getOptimalImageUrl(images))
                }
            }
            else -> throw RuntimeException("Cant handle URI: $uri");
        }
    }

    private fun getOptimalImageUrl(imageList: List<SpotifyImage>): String {
        if (imageList.size == 1) {
            return imageList[0].url
        }
        val sortedImageList = imageList.sortedBy { it.width }
        return sortedImageList[1].url
    }

    private fun downloadImage(uri: SpotifyUri, url: String): Bitmap {
        val bitmap = BitmapFactory.decodeStream(URL(url).openStream())
        imageCache[uri] = bitmap
        return bitmap
    }
}