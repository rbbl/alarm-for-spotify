package cc.rbbl.alarmforspotify.spotify.browser

import cc.rbbl.alarmforspotify.typeAsString
import com.adamratzman.spotify.models.ShowUri

class LibraryShowsFragment : AbstractSpotifySelectionListFragment() {
    override fun setAdapterUpdate(adapter: SpotifySelectionListAdapter) {
        spotifyBrowserViewModel.getSavedShows().observe(
            viewLifecycleOwner,
            { newItemList: List<SimpleSpotifyListItem?>? -> adapter.updateList(newItemList) })
    }

    override fun fetchListItems(limit: Int, offset: Int) {
        spotifyBrowserViewModel.fetchOffsetList(limit, offset, ShowUri.typeAsString())
    }

    override fun getItemListSize(): Int {
        return spotifyBrowserViewModel.getSavedShows().value!!.size
    }
}