package cc.rbbl.alarmforspotify.spotify.browser;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import cc.rbbl.alarmforspotify.R;
import cc.rbbl.alarmforspotify.databinding.ActivitySpotifyBrowserBinding;

public class SpotifyBrowserActivity extends AppCompatActivity {

    public static final String ARG_SPOTIFY_URI = "cc.rbbl.alarmforspotify.spotify.SpotifyBrowserActivity.SpotifyURI";
    private static final String BOTTOM_NAV_PREFERENCE = "rbbl.alarmforspotify.spotify.SpotifyBrowserActivity.bottomNavPreference";

    private LibraryFragment libraryFragment;
    private SearchFragment searchFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivitySpotifyBrowserBinding binding = ActivitySpotifyBrowserBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.bottomMenu.setOnItemSelectedListener(item -> {
            if (item.getItemId() == R.id.spotify_browser_search_menu_item) {
                storeFragmentPreference(item.getItemId());
                if (searchFragment == null) {
                    searchFragment = new SearchFragment();
                }
                changeFragment(searchFragment);
                return true;
            }
            if (item.getItemId() == R.id.spotify_browser_library_menu_item) {
                storeFragmentPreference(item.getItemId());
                if (libraryFragment == null) {
                    libraryFragment = new LibraryFragment();
                }
                changeFragment(libraryFragment);
                return true;
            }
            return false;
        });
        binding.bottomMenu.setSelectedItemId(getFragmentPreference());
    }

    private void storeFragmentPreference(int id) {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(BOTTOM_NAV_PREFERENCE, id);
        editor.apply();
    }

    private int getFragmentPreference() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        return preferences.getInt(BOTTOM_NAV_PREFERENCE, R.id.spotify_browser_search_menu_item);
    }

    private void changeFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.browse_fragment_holder, fragment);
        transaction.commit();
    }
}