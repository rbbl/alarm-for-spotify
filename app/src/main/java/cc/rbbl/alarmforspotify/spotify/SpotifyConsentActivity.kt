package cc.rbbl.alarmforspotify.spotify

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import cc.rbbl.alarmforspotify.R
import cc.rbbl.alarmforspotify.databinding.ActivitySpotifyConsentBinding
import cc.rbbl.alarmforspotify.spotify.viewmodels.SpotifyConsentViewModel
import com.adamratzman.spotify.*
import com.spotify.sdk.android.auth.AuthorizationResponse
import kotlinx.coroutines.runBlocking

class SpotifyConsentActivity : AppCompatActivity() {
    private var mViewModel: SpotifyConsentViewModel? = null
    private var scopes: List<SpotifyScope>? = SpotifyScope.values().toList() //todo limit scopes
    private lateinit var binding: ActivitySpotifyConsentBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySpotifyConsentBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        mViewModel = ViewModelProvider(this)[SpotifyConsentViewModel::class.java]
        if (isSpotifyPkceAuthIntent(intent, mViewModel!!.redirectUri)) {
            runBlocking { handleSpotifyAuthenticationResponse(AuthorizationResponse.fromUri(intent?.data)) }
        }
    }

    override fun onResume() {
        super.onResume()
        if (isSpotifyPkceAuthIntent(intent, mViewModel!!.redirectUri)) {
            runBlocking { handleSpotifyAuthenticationResponse(AuthorizationResponse.fromUri(intent?.data)) }
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        if (intent.data != null) {
            setIntent(intent)
        }
    }


    private suspend fun handleSpotifyAuthenticationResponse(response: AuthorizationResponse) {
        Log.i(this.javaClass.name, "Got pkce auth response of ${response.type}")
        if (response.type != AuthorizationResponse.Type.CODE) {
            if (response.type == AuthorizationResponse.Type.TOKEN ||
                response.type == AuthorizationResponse.Type.ERROR ||
                response.type == AuthorizationResponse.Type.EMPTY ||
                response.type == AuthorizationResponse.Type.UNKNOWN
            ) {
                Log.e(this.javaClass.name, "Got invalid response type... executing error handler")
                onFailure(
                    IllegalStateException("Received response type ${response.type} which is not code.")
                )
            }

            finish()
        } else {
            val authorizationCode = response.code
            if (authorizationCode.isNullOrBlank()) {
                Log.e(this.javaClass.name, "Auth code was null or blank... executing error handler")
                onFailure(
                    IllegalStateException("Authorization code was null or blank.")
                )
            } else {

                try {
                    Log.i(this.javaClass.name, "Building client PKCE api...")
                    val api = spotifyClientPkceApi(
                        clientId = mViewModel!!.clientId,
                        redirectUri = mViewModel!!.redirectUri,
                        authorization = SpotifyUserAuthorization(
                            authorizationCode = authorizationCode,
                            pkceCodeVerifier = mViewModel!!.pkceCodeVerifier
                        )
                    ) {}.build()

                    Log.i(this.javaClass.name, "Successfully built client PKCE api")
                    if (api.token.accessToken.isNotBlank()) {
                        mViewModel!!.credentialStore.spotifyToken = api.token
                        Log.i(
                            this.javaClass.name,
                            "Successful PKCE auth. Executing success handler.."
                        )
                        onSuccess(api)
                    } else {
                        Log.e(
                            this.javaClass.name,
                            "Failed PKCE auth - API token was blank. Executing success handler.."
                        )
                        onFailure(
                            IllegalArgumentException("API token was blank")
                        )
                    }
                } catch (exception: Exception) {
                    Log.e(
                        this.javaClass.name,
                        "Got error in authorization... executing error handler"
                    )
                    onFailure(exception)
                }
            }
        }
    }

    private fun getAuthorizationUrl(): Uri = getSpotifyPkceAuthorizationUrl(
        *scopes!!.toTypedArray(),
        clientId = mViewModel!!.clientId,
        redirectUri = mViewModel!!.redirectUri,
        codeChallenge = getSpotifyPkceCodeChallenge(mViewModel!!.pkceCodeVerifier),
        state = mViewModel!!.state
    ).let { Uri.parse(it) }

    private fun onSuccess(spotifyApi: SpotifyClientApi) {
        mViewModel?.credentialStore?.setSpotifyApi(spotifyApi)
        binding.consentContinue.isEnabled = true
    }

    private fun onFailure(e: Exception) {
        Log.e(this.javaClass.name, "Error during Authorization", e)
    }

    private fun isSpotifyPkceAuthIntent(intent: Intent, redirectUri: String): Boolean {
        return intent.dataString?.startsWith("$redirectUri/?code=") == true || intent.dataString?.startsWith(
            "$redirectUri/?error="
        ) == true
    }

    fun onClickConsent(view: View?) {
        startActivity(Intent(Intent.ACTION_VIEW, getAuthorizationUrl()))
    }

    fun onClickContinue(view: View?) {
        finish()
    }
}
