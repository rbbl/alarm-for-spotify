package cc.rbbl.alarmforspotify;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import cc.rbbl.alarmforspotify.databinding.AlarmListItemBinding;
import cc.rbbl.alarmforspotify.persistence.AlarmAndSpotify;

public class AlarmViewHolder extends RecyclerView.ViewHolder {
    private long entityId;
    private AlarmListItemBinding binding;
    private TextView[] weekDays = new TextView[7];

    public AlarmViewHolder(@NonNull View itemView, AlarmListAdapter alarmListAdapter) {
        super(itemView);
        binding = AlarmListItemBinding.bind(itemView);
        binding.activeToggleButtonListItem.setOnCheckedChangeListener((buttonView, isChecked) -> {
            alarmListAdapter.setActiveAndSave(getAdapterPosition(), isChecked);
        });
        itemView.setOnClickListener(v -> {
            Context context = itemView.getContext();
            Intent intent = new Intent(context, AlarmEditActivity.class);
            intent.putExtra(AlarmEditActivity.EXTRA_ALARM_ID, entityId);
            context.startActivity(intent);
        });
        weekDays[0] = binding.listMonday;
        weekDays[1] = binding.listTuesday;
        weekDays[2] = binding.listWednesday;
        weekDays[3] = binding.listThursday;
        weekDays[4] = binding.listFriday;
        weekDays[5] = binding.listSaturday;
        weekDays[6] = binding.listSunday;
        if(itemView.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            binding.spotifyListTitle.setVisibility(View.VISIBLE);
        }
    }

    public void bind(AlarmAndSpotify entity) {
        entityId = entity.alarmEntity.getId();
        binding.activeToggleButtonListItem.setChecked(entity.alarmEntity.isActive());
        binding.timeListItem.setText(entity.alarmEntity.getTime().toString());
        if (entity.spotifyEntity != null) {
            binding.spotifyListImage.setImageBitmap(entity.spotifyEntity.getIcon());
            binding.spotifyListTitle.setText(entity.spotifyEntity.getTitle());
        }else{
            binding.spotifyListImage.setImageResource(R.drawable.ic_baseline_music_note_24);
            binding.spotifyListTitle.setText("");
        }
        int primaryColor = getPrimaryColor();
        int disabledColor = itemView.getContext().getColor(R.color.disabled);
        int i = 0;
        for (boolean active : entity.alarmEntity.getRepeatingModel()) {
            int color = active ? primaryColor : disabledColor;
            weekDays[i].setTextColor(color);
            i++;
        }
    }

    private int getPrimaryColor() {
        TypedValue typedValue = new TypedValue();
        itemView.getContext().getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
        return typedValue.data;
    }

    static AlarmViewHolder create(ViewGroup parent, AlarmListAdapter alarmListAdapter) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.alarm_list_item, parent, false);
        return new AlarmViewHolder(view, alarmListAdapter);
    }
}
