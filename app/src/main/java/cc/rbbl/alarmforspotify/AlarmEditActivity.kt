package cc.rbbl.alarmforspotify

import android.app.Dialog
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Intent
import android.os.Bundle
import android.text.format.DateFormat
import android.view.View
import android.widget.CompoundButton
import android.widget.TimePicker
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import cc.rbbl.alarmforspotify.activity_contracts.PickSpotifyItem
import cc.rbbl.alarmforspotify.databinding.ActivityAlarmEditBinding
import cc.rbbl.alarmforspotify.models.RepeatingModel
import cc.rbbl.alarmforspotify.persistence.AlarmEntity
import cc.rbbl.alarmforspotify.persistence.SpotifyEntity
import cc.rbbl.alarmforspotify.spotify.SpotifyConsentActivity
import cc.rbbl.alarmforspotify.spotify.SpotifyCredentialStoreHolder
import cc.rbbl.alarmforspotify.viewmodels.AlarmEditViewModel
import com.adamratzman.spotify.models.SpotifyUri
import com.google.android.material.button.MaterialButton
import java.time.LocalTime

class AlarmEditActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAlarmEditBinding
    private var browserLauncher: ActivityResultLauncher<Void?>? = null
    private lateinit var mViewModel: AlarmEditViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAlarmEditBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        mViewModel = ViewModelProvider(this).get(AlarmEditViewModel::class.java)
        val passedId = intent.getLongExtra(EXTRA_ALARM_ID, -1)
        if (passedId != -1L) {
            mViewModel.loadByID(passedId)
        }
        handleSharedContent()
        binding.editTime.text = mViewModel.alarm.value!!.time.toString()
        binding.activeToggleButton.isChecked = mViewModel.alarm.value!!.isActive
        binding.activeToggleButton.setOnCheckedChangeListener { _: CompoundButton?, isChecked: Boolean ->
            mViewModel.alarm.value!!.isActive = isChecked
        }
        binding.spotifyRepeatSwitch.addOnCheckedChangeListener { _, isChecked ->
            mViewModel.alarm.value?.isRepeat = isChecked
        }
        binding.spotifyShuffleSwitch.addOnCheckedChangeListener { _, isChecked ->
            mViewModel.alarm.value?.isShuffle = isChecked
        }
        mViewModel.alarm.observe(this) { alarmEntity: AlarmEntity ->
            binding.editTime.text = alarmEntity.time.toString()
            binding.activeToggleButton.isChecked = alarmEntity.isActive
            binding.editMonday.isChecked =
                alarmEntity.repeatingModel.isActive(RepeatingModel.WeekDays.MONDAY)
            binding.editTuesday.isChecked =
                alarmEntity.repeatingModel.isActive(RepeatingModel.WeekDays.TUESDAY)
            binding.editWednesday.isChecked =
                alarmEntity.repeatingModel.isActive(RepeatingModel.WeekDays.WEDNESDAY)
            binding.editThursday.isChecked =
                alarmEntity.repeatingModel.isActive(RepeatingModel.WeekDays.THURSDAY)
            binding.editFriday.isChecked =
                alarmEntity.repeatingModel.isActive(RepeatingModel.WeekDays.FRIDAY)
            binding.editSaturday.isChecked =
                alarmEntity.repeatingModel.isActive(RepeatingModel.WeekDays.SATURDAY)
            binding.editSunday.isChecked =
                alarmEntity.repeatingModel.isActive(RepeatingModel.WeekDays.SUNDAY)

            binding.spotifyShuffleSwitch.isChecked=alarmEntity.isShuffle
            binding.spotifyRepeatSwitch.isChecked = alarmEntity.isRepeat
        }
        mViewModel.spotifyEntity.observe(this) { spotifyEntity: SpotifyEntity? ->
            if (spotifyEntity?.title != null && spotifyEntity.title.isNotEmpty()) {
                binding.spotifyTitle.text = spotifyEntity.title
            }
            if (spotifyEntity?.creator != null && spotifyEntity.creator.isNotEmpty()) {
                binding.spotifyCreator.text = spotifyEntity.creator
            }
            if (spotifyEntity?.uri != null && spotifyEntity.uri.isNotEmpty()) {
                binding.spotifyUri.text = spotifyEntity.uri
            }
            if (spotifyEntity?.icon != null) {
                binding.spotifyPicture.setImageBitmap(spotifyEntity.icon)
            }
        }
        mViewModel.askForConsent.observe(this) { consentNeeded: Boolean ->
            if (consentNeeded) {
                mViewModel.askForConsent.value = false
                startActivity(Intent(this, SpotifyConsentActivity::class.java))
            }
        }
        initializeWeekView(mViewModel)
        if (!isSpotifyEnabled) {
            startActivity(Intent(this, SpotifyConsentActivity::class.java))
        }
        browserLauncher = registerForActivityResult(PickSpotifyItem()) { result: SpotifyUri? ->
            if (result == null) {
                return@registerForActivityResult
            }
            val spotifyEntity = mViewModel.spotifyEntity.value
            if (spotifyEntity != null) {
                spotifyEntity.uri = result.uri
                mViewModel.spotifyEntity.postValue(spotifyEntity)
                mViewModel.alarm.value!!.uri = result.uri
                mViewModel.fetchSpotifyData()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (!isSpotifyEnabled) {
            startActivity(Intent(this, SpotifyConsentActivity::class.java))
        }
    }

    private fun handleSharedContent() {
        val intent = intent
        val action = intent.action
        val type = intent.type
        if (Intent.ACTION_SEND == action && type != null) {
            if ("text/*" == type || "text/plain" == type) {
                var uri: String? = null
                val input = intent.getStringExtra(Intent.EXTRA_TEXT)
                val urlResult = input?.let { SPOTIFY_URL_PATTERN.find(it) }
                if (urlResult != null) {
                    var link = urlResult.value
                    link = link.split("?").toTypedArray()[0].replace(SPOTIFY_DOMAIN, "")
                    val linkParts = link.split("/").toTypedArray()
                    uri = "spotify:" + linkParts[0] + ":" + linkParts[1]
                }
                val uriResult = input?.let { SPOTIFY_URI_PATTERN.find(it) }
                if (uriResult != null) {
                    uri = uriResult.value
                }
                if (uri != null) {
                    mViewModel.alarm.value!!.uri = uri
                    val spotifyEntity = mViewModel.spotifyEntity.value
                    spotifyEntity!!.uri = uri
                    mViewModel.spotifyEntity.postValue(spotifyEntity)
                    mViewModel.fetchSpotifyData()
                } else {
                    Toast.makeText(this, R.string.shared_handleable_content, Toast.LENGTH_LONG)
                        .show()
                }
            }
        }
    }

    private fun initializeWeekView(viewModel: AlarmEditViewModel) {
        val entity = viewModel.alarm.value
        binding.editMonday.isChecked =
            entity!!.repeatingModel.isActive(RepeatingModel.WeekDays.MONDAY)
        binding.editMonday.addOnCheckedChangeListener { _: MaterialButton?, isChecked: Boolean ->
            mViewModel.alarm.value!!
                .repeatingModel.setActive(RepeatingModel.WeekDays.MONDAY, isChecked)
        }
        binding.editTuesday.isChecked =
            entity.repeatingModel.isActive(RepeatingModel.WeekDays.TUESDAY)
        binding.editTuesday.addOnCheckedChangeListener { _: MaterialButton?, isChecked: Boolean ->
            mViewModel.alarm.value!!
                .repeatingModel.setActive(RepeatingModel.WeekDays.TUESDAY, isChecked)
        }
        binding.editWednesday.isChecked =
            entity.repeatingModel.isActive(RepeatingModel.WeekDays.WEDNESDAY)
        binding.editWednesday.addOnCheckedChangeListener { _: MaterialButton?, isChecked: Boolean ->
            mViewModel.alarm.value!!
                .repeatingModel.setActive(RepeatingModel.WeekDays.WEDNESDAY, isChecked)
        }
        binding.editThursday.isChecked =
            entity.repeatingModel.isActive(RepeatingModel.WeekDays.THURSDAY)
        binding.editThursday.addOnCheckedChangeListener { _: MaterialButton?, isChecked: Boolean ->
            mViewModel.alarm.value!!
                .repeatingModel.setActive(RepeatingModel.WeekDays.THURSDAY, isChecked)
        }
        binding.editFriday.isChecked =
            entity.repeatingModel.isActive(RepeatingModel.WeekDays.FRIDAY)
        binding.editFriday.addOnCheckedChangeListener { _: MaterialButton?, isChecked: Boolean ->
            mViewModel.alarm.value!!
                .repeatingModel.setActive(RepeatingModel.WeekDays.FRIDAY, isChecked)
        }
        binding.editSaturday.isChecked =
            entity.repeatingModel.isActive(RepeatingModel.WeekDays.SATURDAY)
        binding.editSaturday.addOnCheckedChangeListener { _: MaterialButton?, isChecked: Boolean ->
            mViewModel.alarm.value!!
                .repeatingModel.setActive(RepeatingModel.WeekDays.SATURDAY, isChecked)
        }
        binding.editSunday.isChecked =
            entity.repeatingModel.isActive(RepeatingModel.WeekDays.SUNDAY)
        binding.editSunday.addOnCheckedChangeListener { _: MaterialButton?, isChecked: Boolean ->
            mViewModel.alarm.value!!
                .repeatingModel.setActive(RepeatingModel.WeekDays.SUNDAY, isChecked)
        }
    }

    private val isSpotifyEnabled: Boolean
        get() = SpotifyCredentialStoreHolder.getCredentialStoreInstance(application.applicationContext)
            .canApiBeRefreshed()

    fun showTimePickerDialog(view: View) {
        val newFragment = TimePickerFragment(mViewModel)
        newFragment.show(supportFragmentManager, "timePicker")
    }

    fun openBrowseActivity(view: View) {
        browserLauncher!!.launch(null)
    }

    fun saveAndFinish(view: View) {
        mViewModel.insertOrUpdateAlarm()
        mViewModel.insertOrUpdateSpotify()
        finish()
    }

    class TimePickerFragment(private val viewModel: AlarmEditViewModel?) : DialogFragment(),
        OnTimeSetListener {
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val time = viewModel!!.alarm.value!!.time
            // Create a new instance of TimePickerDialog and return it
            return TimePickerDialog(
                activity, this, time.hour, time.minute,
                DateFormat.is24HourFormat(activity)
            )
        }

        override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
            // Do something with the time chosen by the user
            val alarmEntity = viewModel!!.alarm.value
            alarmEntity!!.time = LocalTime.of(hourOfDay, minute)
            viewModel.setAlarm(alarmEntity)
        }
    }

    companion object {
        const val EXTRA_ALARM_ID = "cc.rbbl.alarmforspotify.EditAlarmActivity.EXTRA_ALARM_ID"
        private val SPOTIFY_URL_PATTERN = Regex("https://open.spotify.com/[^ \\t\\n\\r]*")
        private val SPOTIFY_URI_PATTERN =
            Regex("spotify:(album|artist|episode|playlist|show|track):[a-zA-Z0-9]*")
        private const val SPOTIFY_DOMAIN = "https://open.spotify.com/"
    }
}