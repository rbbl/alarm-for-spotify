package cc.rbbl.alarmforspotify.persistence;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface SpotifyDao {
    @Query("SELECT * FROM spotify_items WHERE uri = :uri")
    SpotifyEntity get(String uri);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrReplace(SpotifyEntity spotifyEntity);

    @Delete
    void delete(SpotifyEntity spotifyEntity);
}
