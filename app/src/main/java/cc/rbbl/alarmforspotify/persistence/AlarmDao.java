package cc.rbbl.alarmforspotify.persistence;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface AlarmDao {

    @Query("SELECT * FROM alarms WHERE id = :id")
    AlarmEntity get(long id);

    @Insert
    void insert(AlarmEntity alarmEntity);

    @Update
    void update(AlarmEntity alarmEntity);

    @Delete
    void delete(AlarmEntity alarms);

    @Query("SELECT * FROM alarms ORDER BY time_of_day_ms")
    LiveData<List<AlarmEntity>> getAllAlarms();

    @Query("DELETE FROM alarms")
    void deleteAll();

    @Transaction
    @Query("SELECT * FROM alarms ORDER BY time_of_day_ms")
    LiveData<List<AlarmAndSpotify>> getAllAlarmsWithSpotifyLive();

    @Transaction
    @Query("SELECT * FROM alarms ORDER BY time_of_day_ms")
    List<AlarmAndSpotify> getAllAlarmsWithSpotify();
}
