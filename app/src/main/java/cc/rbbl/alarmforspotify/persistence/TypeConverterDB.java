package cc.rbbl.alarmforspotify.persistence;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.room.TypeConverter;

import java.io.ByteArrayOutputStream;
import java.time.LocalTime;

import cc.rbbl.alarmforspotify.models.RepeatingModel;

public class TypeConverterDB {
    @TypeConverter
    public static LocalTime msToTime(long value) {
        return LocalTime.ofNanoOfDay(value);
    }

    @TypeConverter
    public static long timeToMs(LocalTime localTime) {
        return localTime == null ? 0 : localTime.toNanoOfDay();
    }

    @TypeConverter
    public static byte repeatingModelToByte(RepeatingModel model) {
        return model == null ? 0 : model.toByte();
    }

    @TypeConverter
    public static RepeatingModel byteToRepeatingModel(byte value) {
        return new RepeatingModel(value);
    }

    @TypeConverter
    public static Bitmap byteArrayToBitmap(byte[] input) {
        return BitmapFactory.decodeByteArray(input, 0, input.length);
    }

    @TypeConverter
    public static byte[] bitmapToByteArray(Bitmap input) {
        if (input == null) {
            return new byte[0];
        }
        ByteArrayOutputStream blob = new ByteArrayOutputStream();
        input.compress(Bitmap.CompressFormat.PNG, 0, blob);
        return blob.toByteArray();
    }
}
