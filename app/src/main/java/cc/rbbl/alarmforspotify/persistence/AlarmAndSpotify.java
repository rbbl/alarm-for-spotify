package cc.rbbl.alarmforspotify.persistence;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.Objects;

public class AlarmAndSpotify {
    @Embedded public AlarmEntity alarmEntity;
    @Relation(
            parentColumn = "spotify_uri",
            entityColumn = "uri"
    )
    public SpotifyEntity spotifyEntity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlarmAndSpotify that = (AlarmAndSpotify) o;
        return Objects.equals(alarmEntity, that.alarmEntity) &&
                Objects.equals(spotifyEntity, that.spotifyEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(alarmEntity, spotifyEntity);
    }
}
