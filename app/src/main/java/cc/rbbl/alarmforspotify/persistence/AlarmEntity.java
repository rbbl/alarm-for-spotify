package cc.rbbl.alarmforspotify.persistence;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.time.LocalTime;
import java.util.Objects;

import cc.rbbl.alarmforspotify.models.RepeatingModel;

@Entity(tableName = "alarms")
public class AlarmEntity {
    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "time_of_day_ms")
    private LocalTime time;

    @ColumnInfo(name = "active")
    private boolean active;

    @ColumnInfo(name = "repeating_model")
    private RepeatingModel repeatingModel = new RepeatingModel();

    //TODO add relation to SpotifyEntity
    @ColumnInfo(name = "spotify_uri")
    private String uri;

    @ColumnInfo(name = "spotify_shuffle")
    private boolean shuffle;

    @ColumnInfo(name = "spotify_repeat")
    private boolean repeat;

    public AlarmEntity() {
        time = LocalTime.now().withSecond(0).withNano(0);
        active = true;
    }

    @Ignore
    public AlarmEntity(LocalTime time) {
        this(time, true);
    }

    @Ignore
    public AlarmEntity(LocalTime time, boolean active) {
        this.time = time;
        this.active = active;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public RepeatingModel getRepeatingModel() {
        return repeatingModel;
    }

    public void setRepeatingModel(RepeatingModel repeatingModel) {
        this.repeatingModel = repeatingModel;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public boolean isShuffle() {
        return shuffle;
    }

    public void setShuffle(boolean shuffle) {
        this.shuffle = shuffle;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlarmEntity entity = (AlarmEntity) o;
        return id == entity.id &&
                active == entity.active &&
                Objects.equals(time, entity.time) &&
                Objects.equals(repeatingModel, entity.repeatingModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, time, active, repeatingModel);
    }
}
