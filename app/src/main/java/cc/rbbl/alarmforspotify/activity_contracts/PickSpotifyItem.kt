package cc.rbbl.alarmforspotify.activity_contracts

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import cc.rbbl.alarmforspotify.spotify.browser.SpotifyBrowserActivity
import com.adamratzman.spotify.models.SpotifyUri

class PickSpotifyItem : ActivityResultContract<Void?, SpotifyUri?>() {
    override fun createIntent(context: Context, input: Void?): Intent =
        Intent(context, SpotifyBrowserActivity::class.java)

    override fun parseResult(resultCode: Int, intent: Intent?): SpotifyUri? {
        if (resultCode == Activity.RESULT_OK && intent != null) {
            val result: String? = intent.getStringExtra(SpotifyBrowserActivity.ARG_SPOTIFY_URI)
            return if (result != null) {
                SpotifyUri(result)
            }else {
                null
            }
        }
        return null
    }
}