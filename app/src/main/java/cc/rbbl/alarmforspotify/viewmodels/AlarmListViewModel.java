package cc.rbbl.alarmforspotify.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import cc.rbbl.alarmforspotify.persistence.AlarmAndSpotify;
import cc.rbbl.alarmforspotify.persistence.AlarmDao;
import cc.rbbl.alarmforspotify.persistence.AlarmEntity;
import cc.rbbl.alarmforspotify.persistence.AppDatabase;

public class AlarmListViewModel extends AndroidViewModel {
    private AlarmDao alarmDao;

    private MutableLiveData<List<AlarmAndSpotify>> alarms = new MutableLiveData<>();

    public AlarmListViewModel(Application application) {
        super(application);
        alarmDao = AppDatabase.getDatabase(application).alarmDao();
    }

    public void update(AlarmEntity entity) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            alarmDao.update(entity);
        });
    }

    public void delete(AlarmEntity entity) {
        AppDatabase.databaseWriteExecutor.execute(() ->{
            alarmDao.delete(entity);
            reFetchData();
        });
    }

    public void reFetchData() {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            alarms.postValue(alarmDao.getAllAlarmsWithSpotify());
        });
    }

    public LiveData<List<AlarmAndSpotify>> getAlarms() {
        return alarms;
    }
}