package cc.rbbl.alarmforspotify.viewmodels

import android.app.Application
import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import cc.rbbl.alarmforspotify.persistence.*
import cc.rbbl.alarmforspotify.spotify.SpotifyCredentialStoreHolder
import com.adamratzman.spotify.SpotifyClientApi
import kotlinx.coroutines.launch
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class AlarmEditViewModel(application: Application?) : AndroidViewModel(application!!) {
    private val alarmDao: AlarmDao
    private val spotifyDao: SpotifyDao
    private var mSpotifyClientApi: SpotifyClientApi? = null
    private val _alarm = MutableLiveData<AlarmEntity>()
    val spotifyEntity = MutableLiveData<SpotifyEntity?>()
    val askForConsent = MutableLiveData<Boolean>()
    private fun initializeSpotifyApi() {
        mSpotifyClientApi =
            SpotifyCredentialStoreHolder.getCredentialStoreInstance(getApplication<Application>().applicationContext)
                .getSpotifyClientPkceApi(null)
    }

    var alarm = _alarm as LiveData<AlarmEntity>

    fun setAlarm(entity: AlarmEntity) {
        _alarm.value = entity
    }

    fun fetchSpotifyData() = viewModelScope.launch {
        if (spotifyEntity.value!!.uri == null) {
            return@launch
        }
        val uriParts = spotifyEntity.value!!.uri.split(":").toTypedArray()
        when (uriParts[1].lowercase()) {
            "album" -> {
                val album = mSpotifyClientApi!!.albums.getAlbum(uriParts[2])
                if (album != null) {
                    val entity = spotifyEntity.value
                    downLoadAndSetImage(album.images[0].url)
                    entity!!.title = album.name
                    entity.creator = album.artists[0].name //TODO support multi artist
                    spotifyEntity.postValue(entity)
                } else {
                    //todo error handling
                }
            }
            "artist" -> {
                val artist = mSpotifyClientApi!!.artists.getArtist(uriParts[2])
                if (artist != null) {
                    val entity = spotifyEntity.value
                    downLoadAndSetImage(artist.images[0].url)
                    entity!!.title = artist.name
                    entity.creator = artist.name
                    spotifyEntity.postValue(entity)
                } else {
                    //todo error handling
                }
            }
            "episode" -> {
                val episode = mSpotifyClientApi!!.episodes.getEpisode(uriParts[2])
                if (episode != null) {
                    val entity = spotifyEntity.value
                    downLoadAndSetImage(episode.images[0].url)
                    entity!!.title = episode.name
                    entity.creator = episode.show.name
                    spotifyEntity.postValue(entity)
                } else {
                    //todo error handling
                }
            }
            "playlist" -> {
                val playlist = mSpotifyClientApi!!.playlists.getPlaylist(uriParts[2])
                if (playlist != null) {
                    val entity = spotifyEntity.value
                    downLoadAndSetImage(playlist.images[0].url)
                    entity!!.title = playlist.name
                    entity.creator = playlist.owner.displayName
                    spotifyEntity.postValue(entity)
                } else {
                    //todo error handling
                }
            }
            "show" -> {
                val show = mSpotifyClientApi!!.shows.getShow(uriParts[2])
                if (show != null) {
                    val entity = spotifyEntity.value
                    downLoadAndSetImage(show.images[0].url)
                    entity!!.title = show.name
                    entity.creator = show.publisher
                    spotifyEntity.postValue(entity)
                } else {
                    //todo error handling
                }
            }
            "track" -> {
                val track = mSpotifyClientApi!!.tracks.getTrack(uriParts[2])
                if (track != null) {
                    val entity = spotifyEntity.value
                    val album = mSpotifyClientApi!!.albums.getAlbum(track.album.id)
                    if (album != null) {
                        downLoadAndSetImage(album.images[0].url)
                    } else {
                        //todo error handling
                    }
                    entity!!.title = track.name
                    entity.creator = track.artists[0].name //TODO support multi artist
                    spotifyEntity.postValue(entity)
                } else {
                    //todo error handling
                }
            }
            else -> throw RuntimeException(
                "Cant handle URI: " + spotifyEntity.value!!
                    .uri
            )
        }
    }

    private fun downLoadAndSetImage(url: String) {
        AppDatabase.databaseWriteExecutor.execute {
            val input: InputStream? = null
            var connection: HttpURLConnection? = null
            try {
                val source = URL(url)
                connection = source.openConnection() as HttpURLConnection
                connection.connect()

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.responseCode != HttpURLConnection.HTTP_OK) {
                    return@execute
                }
                val spotifyEntity = spotifyEntity.value
                // download the file
                spotifyEntity!!.icon = BitmapFactory.decodeStream(connection.inputStream)
                this.spotifyEntity.postValue(spotifyEntity)
            } catch (e: Exception) {
                Log.e("DOWNLOAD", e.message, e)
                return@execute
            } finally {
                connection?.disconnect()
            }
        }
    }

    fun loadByID(id: Long) {
        AppDatabase.databaseWriteExecutor.execute {
            val alarmEntity = alarmDao[id]
            _alarm.postValue(alarmEntity)
            val spotifyEntity = spotifyDao[alarmEntity.uri]
            if (spotifyEntity != null) {
                this.spotifyEntity.postValue(spotifyEntity)
            }
        }
    }

    fun insertOrUpdateAlarm() {
        if (_alarm.value!!.id == 0L) {
            AppDatabase.databaseWriteExecutor.execute { alarmDao.insert(_alarm.value) }
        } else {
            AppDatabase.databaseWriteExecutor.execute { alarmDao.update(_alarm.value) }
        }
    }

    fun insertOrUpdateSpotify() {
        if (spotifyEntity.value!!.uri != null && spotifyEntity.value!!.uri != "") {
            AppDatabase.databaseWriteExecutor.execute { spotifyDao.insertOrReplace(spotifyEntity.value) }
        }
    }

    init {
        initializeSpotifyApi()
        alarmDao = AppDatabase.getDatabase(application).alarmDao()
        spotifyDao = AppDatabase.getDatabase(application).spotifyDao()
        _alarm.value = AlarmEntity()
        spotifyEntity.value = SpotifyEntity()
        askForConsent.value = false
    }
}