package cc.rbbl.alarmforspotify;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cc.rbbl.alarmforspotify.persistence.AlarmAndSpotify;
import cc.rbbl.alarmforspotify.persistence.AlarmEntity;
import cc.rbbl.alarmforspotify.viewmodels.AlarmListViewModel;

public class AlarmListAdapter extends RecyclerView.Adapter<AlarmViewHolder> {
    private AlarmListViewModel alarmListViewModel;
    private List<AlarmAndSpotify> itemList = new ArrayList<>();

    public AlarmListAdapter(AlarmListViewModel viewModel) {
        alarmListViewModel = viewModel;
    }

    public void deleteItem(int position) {
        AlarmEntity alarmEntity = alarmListViewModel.getAlarms().getValue().get(position).alarmEntity;
        alarmListViewModel.delete(alarmEntity);
    }

    public void setActiveAndSave(int position, boolean active) {
        AlarmAndSpotify entity = itemList.get(position);
        entity.alarmEntity.setActive(active);
        alarmListViewModel.update(entity.alarmEntity);
    }

    @Override
    public AlarmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return AlarmViewHolder.create(parent, this);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public void onBindViewHolder(AlarmViewHolder holder, int position) {
        AlarmAndSpotify current = itemList.get(position);
        holder.bind(current);
    }

    public void updateAlarmListItems(List<AlarmAndSpotify> updatedAlarms) {
        final AlarmDiff diffCallback = new AlarmDiff(this.itemList, updatedAlarms);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.itemList.clear();
        this.itemList.addAll(updatedAlarms);
        diffResult.dispatchUpdatesTo(this);
    }

    public static class AlarmDiff extends DiffUtil.Callback {

        private final List<AlarmAndSpotify> mOldAlarmList;
        private final List<AlarmAndSpotify> mNewAlarmList;

        public AlarmDiff(List<AlarmAndSpotify> oldAlarmList, List<AlarmAndSpotify> newAlarmList) {
            this.mOldAlarmList = oldAlarmList;
            this.mNewAlarmList = newAlarmList;
        }

        @Override
        public int getOldListSize() {
            return mOldAlarmList.size();
        }

        @Override
        public int getNewListSize() {
            return mNewAlarmList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return mOldAlarmList.get(oldItemPosition).alarmEntity.getId() == mNewAlarmList.get(
                    newItemPosition).alarmEntity.getId();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            final AlarmAndSpotify oldAlarm = mOldAlarmList.get(oldItemPosition);
            final AlarmAndSpotify newAlarm = mNewAlarmList.get(newItemPosition);

            return oldAlarm.equals(newAlarm);
        }

        @Nullable
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            // Implement method if you're going to use ItemAnimator
            return super.getChangePayload(oldItemPosition, newItemPosition);
        }
    }

    public static class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback {
        private AlarmListAdapter mAdapter;

        public SwipeToDeleteCallback(AlarmListAdapter adapter) {
            super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
            mAdapter = adapter;
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            mAdapter.deleteItem(position);
        }
    }
}
